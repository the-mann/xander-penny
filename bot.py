from typing import Optional

import discord
import sqlalchemy

from discord import Reaction, User, TextChannel, Message, Guild, CategoryChannel
from discord.ext import commands
from discord.ext.commands import Cog, Context, UserInputError
from sqlalchemy import create_engine, func, text
from sqlalchemy.orm import sessionmaker, Query

from schema.Campaign import Campaign
from schema.Transaction import Transaction
from schema.base import Base

DISCORD_TOKEN = 'xxxxx'
XP_EMOJI_FORMATTED = '<:xpenny:603994187494457556>'
XP_EMOJI_NAME = 'xpenny'
DEBUG = True
XANDER_ID = 326391099675377676
XANDER_FORMATTED = f'<@{XANDER_ID}>'

if 'xxxxx' in [DISCORD_TOKEN, XP_EMOJI_FORMATTED, XP_EMOJI_NAME, XANDER_ID]:
    raise Exception("Token not set")

Session = sessionmaker()


class NotXanderError(UserInputError):
    """This poor user is not Xander, and therefore can't give out bulk Xander Pennies"""
    pass

class NoCategoryFoundError(UserInputError):
    """Either the user didn't provide a category or isn't in a category."""
    pass

class GiveToYourselfError(UserInputError):
    pass


class Commands(Cog):
    # Commands
    def __init__(self):
        self.session = Session()
        super(Commands, self).__init__()

    @commands.command()
    async def give(self, ctx: Context, users: commands.Greedy[discord.User], amount: int):
        guild = ctx.guild  # type: Guild
        print(users)
        print(amount)
        if ctx.author != guild.get_member(XANDER_ID):
            raise NotXanderError
        elif ctx.author == users:
            raise GiveToYourselfError
        else:
            for user in users:
                self.session.add(Transaction(giver_id=ctx.author.id, reciever_id=user.id, amount=amount))
            self.session.commit()
            await ctx.send(f"As the mighty {XANDER_FORMATTED} wishes.")

    @give.error
    async def give_error(self, ctx: Context, error):
        if isinstance(error, (commands.BadArgument, commands.errors.MissingRequiredArgument)):
            await ctx.send(f'Usage: $give @<user1> @<user2> .... \<integer amount of {XP_EMOJI_FORMATTED}\>')
        elif isinstance(error, NotXanderError):
            await ctx.send(f'You dare assume the power of the great {XANDER_FORMATTED}? 💢😡😡😡')
        elif isinstance(error, GiveToYourselfError):
            await ctx.send(f'Silly {XANDER_FORMATTED}, you can\'t give to yourself 🙄')
        else:
            print(type(error))

    @commands.command()
    async def score(self, ctx, user: Optional[User] = None):
        """Get the score of yourself (or another user)
        Usage: $score {@user}"""
        if user is None:
            user = ctx.author
        await ctx.send(f'{self.get_score(user)} {XP_EMOJI_FORMATTED}')

    @commands.command()
    async def leaderboard(self, ctx, amount_of_users: Optional[int] = 10):
        """Displays the leaderboard
         Usage: $leaderboard {amount_of_users}"""
        guild = ctx.guild #type: Guild
        msg = ''
        for index, row in enumerate(self.get_leaderboard(amount_of_users)):
            user = await guild.fetch_member(row[1])
            print(row[1])
            msg += f'**{index + 1}. {user.display_name}:** {row[0]} {XP_EMOJI_FORMATTED}\n'
        await ctx.send(msg)

    @commands.command()
    async def claim(self, ctx):
        guild = ctx.guild #type: Guild
        category_id = ctx.message.channel.category_id
        if category_id is None:
            raise NoCategoryFoundError()
        category = guild.get_channel(category_id)

        campaign = self.session.query(Campaign).filter(Campaign.channel_category_id == category.id).first()
        if campaign:
            dm = await guild.fetch_member(campaign.dm_id)
            await ctx.send(f"{dm.display_name} already DM's this campaign 😐")
        else:
            self.session.add(Campaign(dm_id=ctx.author.id, channel_category_id=category_id))
            self.session.commit()
            await ctx.send(f"You have successfully claimed {category}. Use `$addreward` to add rewards to this campaign. Be sure to use it under the campaign's channel category.")
    def get_score(self, user: User):
        q = self.session.query(func.sum(Transaction.amount).label("amount")).group_by(Transaction.reciever_id).filter(
            Transaction.reciever_id == user.id)  # type: Query
        result = q.first()
        if not result:
            return 0
        else:
            return q.first()[0]

    def get_leaderboard(self, amount_of_users):
        q = self.session.query(func.sum(Transaction.amount).label("amount"), Transaction.reciever_id).group_by(
            Transaction.reciever_id).order_by(text("amount desc")).limit(amount_of_users)
        return q.all()


class MyClient(commands.Bot):
    def __init__(self):
        self.session = Session()
        super(MyClient, self).__init__('$')
        self.add_cog(Commands())

    async def on_ready(self):
        print('Logged on as {0}!'.format(self.user))

    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        channel = self.get_channel(payload.channel_id)  # type: TextChannel
        giver = self.get_user(payload.user_id)
        message = await channel.fetch_message(payload.message_id)  # type: Message
        reciever = message.author
        print(payload.emoji)
        print(payload.emoji.name)
        if payload.emoji.name == XP_EMOJI_NAME and giver != reciever and reciever != self.user:
            # XP has been given

            if DEBUG:
                await channel.send(f'<@!{giver.id}> gave {payload.emoji} to <@!{reciever.id}>')
            # Create transaction in database
            self.session.add(Transaction(giver_id=giver.id, reciever_id=reciever.id, message_id=message.id, amount=1))
            self.session.commit()


engine = create_engine('sqlite:///test.db', echo=True)
Base.metadata.create_all(engine)
Session.configure(bind=engine)
client = MyClient()

client.run(DISCORD_TOKEN)
