from sqlalchemy import Column, Integer, String, Sequence, ForeignKey
from sqlalchemy.orm import relationship

from schema.Campaign import Campaign
from schema.base import Base


class UserReward(Base):
    __tablename__ = 'user_rewards'
    id = Column(Integer, Sequence('user_reward_id_seq'), primary_key=True)

    buyer_id = Column(String, nullable=False)
    reward_id = Column(Integer, ForeignKey('rewards.id'), nullable=False)

    reward = relationship("Reward", back_populates="rewarded")