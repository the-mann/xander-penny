
from sqlalchemy import Column, Integer, String, Sequence

from schema.base import Base


class Transaction(Base):
    __tablename__ = 'transactions'
    id = Column(Integer, Sequence('transaction_id_seq'), primary_key=True)
    giver_id = Column(String, nullable=False)
    reciever_id = Column(String, nullable=False)
    message_id = Column(String)
    amount = Column(Integer, default=1, nullable=False)