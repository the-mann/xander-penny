from sqlalchemy import Column, Integer, String, Sequence

from schema.base import Base


class NegativePenny(Base):
    __tablename__ = 'negative_pennies'
    id = Column(Integer, Sequence('negative_penny_id_seq'), primary_key=True)
    message_id = Column(String, nullable=False)