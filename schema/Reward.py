from sqlalchemy import Column, Integer, String, Sequence, ForeignKey
from sqlalchemy.orm import relationship

from schema.Campaign import Campaign
from schema.base import Base


class Reward(Base):
    __tablename__ = 'rewards'
    id = Column(Integer, Sequence('reward_id_seq'), primary_key=True)
    campaign_id = Column(Integer, ForeignKey('campaigns.id'))
    cost = Column(Integer)
    reciever_id = Column(String)
    message_id = Column(String)

    campaign = relationship("Campaign", back_populates="rewards")