from sqlalchemy import Column, Integer, String, Sequence

from schema.base import Base


class Campaign(Base):
    __tablename__ = 'campaigns'
    id = Column(Integer, Sequence('campaign_id_seq'), primary_key=True)
    dm_id = Column(String)
    channel_category_id = Column(String)